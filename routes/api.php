<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
    Authenticate group
*/
Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController@register');
    Route::post('login', 'LoginController@login');
});
Route::get('test', function ()
{
    return response()->JSON(['1']);
});
Route::group([
    'middleware' => ['auth:api']
], function ()
{
    /*
     *  /api/{route}?api_token=UNIQUE_TOKEN
    */
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('me', 'UserController@getMe');
});

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\TokenGuard as TokenGuard;
use Illuminate\Support\Facades\Auth as Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        // dd(new TokenGuard(Auth::createUserProvider($config['provider']), $app->request()));
        Auth::extend('token', function ($app, $name, array $config) {
            // dd($app->request);
            return new TokenGuard(Auth::createUserProvider($config['provider']), $app->request);
        });
        //
    }
}

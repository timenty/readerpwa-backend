<?php

namespace App\Http\Controllers;

use Auth;
use App\User;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class AuthController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            // Аутентификация прошла успешно
            return redirect()->intended('home');
        }
    }

    public function register()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            // Аутентификация прошла успешно
            return redirect()->intended('home');
        }
    }
}

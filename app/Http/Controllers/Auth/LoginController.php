<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth as Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @OVERRIDE.
     *
     * @return mixed
     */
    protected function sendLoginResponse(Request $request)
    {
        if ($request->expectsJson()) {
            if (!Auth::user()->api_token) {
                Auth::user()->setNewApiToken();
            }
            return $this->authenticated($request, $this->guard()->user()) ?: array('api_token' => Auth::user()->api_token);

        }else{
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);

            return $this->authenticated($request, $this->guard()->user())
                    ?: redirect()->intended($this->redirectPath());
        }
    }

    /**
     * @OVERRIDE.
     *
     * @return mixed
     */
    public function logout(Request $request)
    {
        if ($request->expectsJson()) {

            $request->user()->logout();
            return array('success' => true);
        }else{

            $this->guard()->logout();
            $request->session()->invalidate();
            return $this->loggedOut($request) ?: redirect('/');
        }
    }
}

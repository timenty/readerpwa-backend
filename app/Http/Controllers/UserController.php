<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getMe(Request $request)
    {
        $user = Auth::user();
        // filtration data
        unset($user->created_at);
        unset($user->updated_at);
        unset($user->role_id);
        unset($user->settings);
        unset($user->email_verified_at);

        return $user;
    }
}
